/** Sticky Navigation Menu */

let navbar = $(".navbar");

$(window).scroll(function(){
    // console.log(window.innerHeight);
    // console.log($(".intro-catagory").offset().top);
    let oTop = $("#sticky-nav-appear").offset().top - window.innerHeight;

    if($(window).scrollTop() > oTop) {
        navbar.addClass("sticky");
    } else {
        navbar.removeClass("sticky");
    }

});

/** Animation On Scroll package */
AOS.init();
